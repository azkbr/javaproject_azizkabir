package com.project.javaproject.controller;

import com.project.javaproject.entities.Payment;
import com.project.javaproject.exceptions.PaymentException;
import com.project.javaproject.service.PaymentBs;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class PaymentController {

    private final PaymentBs paymentBs;

    private final Logger logger;

    public PaymentController(PaymentBs paymentBs) {
        this.paymentBs = paymentBs;
        logger = Logger.getLogger(PaymentController.class.getName());
    }

    @GetMapping(value = "/rowCount")
    public ResponseEntity<Integer> rowCount() {
        int count = paymentBs.rowCount();
        logger.info(String.format("Found %d rows", count));
        return ResponseEntity.ok().body(count);
    }

    @GetMapping(value = "/findById/{id}")
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {
        Payment paymentBsById = paymentBs.findById(id);
        logger.info(String.format("Found Payment for id:%d", id));
        return ResponseEntity.ok().body(paymentBsById);
    }

    @GetMapping(value = "/findByType")
    public ResponseEntity<List<Payment>> findByType(@RequestParam("type") String type) {
        List<Payment> paymentList = paymentBs.findByType(type);
        logger.info(String.format("Found Payments for type:%s", type));
        return ResponseEntity.ok().body(paymentList);
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Integer> save(@RequestBody Payment payment) {
        int resultCode = paymentBs.save(payment);
        logger.info(String.format("Saved %s payment id:%d", payment.getType(), payment.getId()));
        return ResponseEntity.ok().body(resultCode);
    }

    @GetMapping(value = "/status")
    public String status() {
        logger.info("Status invoked");
        return "OK";
    }

    //Controller Advice
    @ExceptionHandler({PaymentException.class})
    public ResponseEntity<String> handleException(PaymentException e) {
        logger.warning(String.format("Exception Thrown: %s", e.getMessage()));
        return ResponseEntity.badRequest().body(e.getMessage());
    }

}
