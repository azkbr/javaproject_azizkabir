package com.project.javaproject.utilities;

import com.project.javaproject.entities.Payment;

public class Util {

    Util() {
        //Utils
    }

    //validation method to test object is populated with data.
    public static boolean validatePayment(Payment payment) {
        return payment.getId() > 0
                && payment.getPaymentDate() != null
                && payment.getType() != null
                && payment.getAmount() > 0.0
                && payment.getCustId() > 0;
    }

}
