package com.project.javaproject.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Document
public class Payment {
    @Id
    private int id;
    private LocalDate paymentDate;
    private String type;
    private double amount;
    private int custId;

}
