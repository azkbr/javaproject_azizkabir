package com.project.javaproject.dao;

import com.project.javaproject.entities.Payment;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PaymentDAO extends MongoRepository<Payment, ObjectId> {

     Payment findById(int id);
     List<Payment> findByType(String type);

}
