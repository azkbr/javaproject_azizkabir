package com.project.javaproject.service;


import com.project.javaproject.entities.Payment;

import java.util.List;

public interface IPaymentService{

    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);

}
