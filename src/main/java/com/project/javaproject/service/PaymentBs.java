package com.project.javaproject.service;

import com.project.javaproject.dao.PaymentDAO;
import com.project.javaproject.entities.Payment;
import com.project.javaproject.exceptions.PaymentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.project.javaproject.utilities.Util.validatePayment;

@Service
public class PaymentBs implements IPaymentService {

    private final PaymentDAO paymentDAO;
    private static final String FINDBYID_EXCEPTION_MSG = "ID must be greater than 0!";
    private static final String FINDBYTYPE_EXCEPTION_MSG = "Type must not be Null!";
    private static final String SAVE_EXCEPTION_MSG = "Payment must not be Null!";

    @Autowired
    public PaymentBs(PaymentDAO paymentDAO) {
        this.paymentDAO = paymentDAO;
    }

    @Override
    public int rowCount() {
        return Math.toIntExact(paymentDAO.count()); //Only a learning project so shouldn't overflow int... ^_^
    }

    @Override
    public Payment findById(int id) throws PaymentException {
        if(id>0) {
            return paymentDAO.findById(id);
        } else {
            throw new PaymentException(FINDBYID_EXCEPTION_MSG);
        }
    }

    @Override
    public List<Payment> findByType(String type)  throws PaymentException {
        if (!type.isEmpty() && !type.isBlank()){
            return paymentDAO.findByType(type);
        } else {
            throw new PaymentException(FINDBYTYPE_EXCEPTION_MSG);
        }
    }

    @Override
    public int save(Payment payment) throws PaymentException {

        if (validatePayment(payment)) {
            paymentDAO.save(payment);
            return 1;
        } else {
            throw new PaymentException(SAVE_EXCEPTION_MSG);
        }
    }
}
