package com.project.javaproject.controller;

import com.project.javaproject.entities.Payment;
import com.project.javaproject.service.PaymentBs;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(SpringExtension.class)
class PaymentControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PaymentBs paymentBsMock;

    private PaymentController paymentController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        paymentController = new PaymentController(paymentBsMock);
        mockMvc = standaloneSetup(paymentController).build();
    }

    @Test
    void getRowCount() throws Exception {
        doReturn(1).when(paymentBsMock).rowCount();

        mockMvc.perform(MockMvcRequestBuilders.get("/rowCount"))
                .andExpect(status().isOk());

        verify(paymentBsMock).rowCount();
    }

    @Test
    void findById_success() throws Exception {
        doReturn(new Payment(1, LocalDate.now(), "VISA", 150.00, 1234)).when(paymentBsMock).findById(anyInt());

        mockMvc.perform(MockMvcRequestBuilders.get("/findById/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.paymentDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.type").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.custId").exists());

        verify(paymentBsMock).findById(anyInt());
    }

    @Test
    void findByType_success() throws Exception {
        doReturn(Collections.singletonList(new Payment(1, LocalDate.now(), "VISA", 150.00, 1234))).when(paymentBsMock).findByType(anyString());

        mockMvc.perform(MockMvcRequestBuilders.get("/findByType")
                .param("type", "VISA"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].paymentDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].type").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].amount").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].custId").exists());

        verify(paymentBsMock).findByType(anyString());
    }

    @Test
    void save_success() throws Exception {
        doReturn(1).when(paymentBsMock).save(any(Payment.class));

        mockMvc.perform(MockMvcRequestBuilders.post("/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"paymentDate\":[2020,9,15],\"type\":\"VISA\",\"amount\":150.0,\"custId\":1234}"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1"));

        verify(paymentBsMock).save(any(Payment.class));
    }

    @Test
    void status_success() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/status"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("OK"));

    }
}