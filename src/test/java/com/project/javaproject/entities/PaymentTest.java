package com.project.javaproject.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentTest {

    private Payment payment;
    private LocalDate localDate;
    @BeforeEach
    void setUp() {
        localDate = LocalDate.now();
        payment = new Payment(1, localDate, "VISA", 150.00, 1234);
    }

    @Test
    void getId() {
    assertEquals(1, payment.getId());
    }

    @Test
    void getPaymentDate() {
        assertEquals(localDate,payment.getPaymentDate());
    }

    @Test
    void getType() {
        assertEquals("VISA", payment.getType());
    }

    @Test
    void getAmount() {
        assertEquals(150.00, payment.getAmount());
    }

    @Test
    void getCustId() { assertEquals(1234, payment.getCustId());
    }

    @Test
    void setId() { payment.setId(999); assertEquals(999, payment.getId());
    }

    @Test
    void setPaymentDate() { LocalDate localDate1 = LocalDate.now();
    payment.setPaymentDate(localDate1);
    assertEquals(localDate1, payment.getPaymentDate());
    }

    @Test
    void setType() {
        payment.setType("MASTERCARD"); assertEquals("MASTERCARD", payment.getType());
    }

    @Test
    void setAmount() { payment.setAmount(999.99); assertEquals(999.99, payment.getAmount());
    }

    @Test
    void setCustId() { payment.setCustId(555); assertEquals(555, payment.getCustId());
    }

    @Test
    void testToString() {
        assertEquals("Payment(id=1, paymentDate=" + localDate + ", type=VISA, amount=150.0, custId=1234)", payment.toString());
    }


}