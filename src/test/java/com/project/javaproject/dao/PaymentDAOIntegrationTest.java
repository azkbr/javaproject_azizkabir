package com.project.javaproject.dao;

import com.project.javaproject.entities.Payment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
@ExtendWith(SpringExtension.class)
public class PaymentDAOIntegrationTest {

    @Autowired
    private PaymentDAO paymentDAO;

    @Test
    public void save_And_findById_Success() {
        Payment payment = new Payment(1, LocalDate.now(), "VISA", 150.00, 1234);

        paymentDAO.save(payment);

        assertEquals(payment, paymentDAO.findById(1));
    }

    @Test
    public void save_And_findByType_Success() {
        Payment payment = new Payment(2, LocalDate.now(), "VISA", 150.00, 1234);

        paymentDAO.save(payment);

        List<Payment> paymentList = paymentDAO.findByType("VISA");
        paymentList.forEach(p ->
                assertEquals("VISA", p.getType()));
    }

    @Test
    public void save_and_rowCount_Success() {
        Payment payment = new Payment(3, LocalDate.now(), "VISA", 150.00, 1234);

        paymentDAO.save(payment);

        assertEquals(1, paymentDAO.count());
    }
}
