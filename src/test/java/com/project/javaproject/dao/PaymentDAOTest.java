package com.project.javaproject.dao;

import com.project.javaproject.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

class PaymentDAOTest {

    @Mock
    private PaymentDAO paymentDAO;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_success() {
        doReturn(1L).when(paymentDAO).count();
        assertEquals(1L, paymentDAO.count());
    }

    @Test
    void findById_success() {
        LocalDate date = LocalDate.now();
        Payment payment = new Payment(1, date, "VISA", 150.00, 1234);
        doReturn(payment).when(paymentDAO).findById(anyInt());
        assertEquals(1, payment.getId());
        assertEquals(date, payment.getPaymentDate());
        assertEquals("VISA", payment.getType());
        assertEquals(150.00, payment.getAmount());
        assertEquals(1234, payment.getCustId());
    }

    @Test
    void findByType_success() {
        LocalDate date = LocalDate.now();
        Payment payment = new Payment(1, date, "MASTERCARD", 150.00, 1234);
        doReturn(payment).when(paymentDAO).findById(anyInt());
        assertEquals(1, payment.getId());
        assertEquals(date, payment.getPaymentDate());
        assertEquals("MASTERCARD", payment.getType());
        assertEquals(150.00, payment.getAmount());
        assertEquals(1234, payment.getCustId());
    }

    @Test
    void save_success() {
        LocalDate date = LocalDate.now();
        doReturn(new Payment(1, date, "MASTERCARD", 150.00, 1234)).when(paymentDAO).save(any(Payment.class));
        Payment payment = new Payment(1, date, "MASTERCARD", 150.00, 1234);

        assertEquals(payment, paymentDAO.save(new Payment(1, date, "MASTERCARD", 150.00, 1234)));

    }
}