package com.project.javaproject.service;

import com.project.javaproject.dao.PaymentDAO;
import com.project.javaproject.entities.Payment;
import com.project.javaproject.exceptions.PaymentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

class PaymentBsTest {

    private static final String FINDBYID_EXCEPTION_MSG = "ID must be greater than 0!";
    private static final String FINDBYTYPE_EXCEPTION_MSG = "Type must not be Null!";
    private static final String SAVE_EXCEPTION_MSG = "Payment must not be Null!";

    @Mock
    private PaymentDAO paymentDAOMock;
    private PaymentBs paymentBs;

    @BeforeEach
    void setUp() {
        initMocks(this);
        paymentBs = new PaymentBs(paymentDAOMock);
    }

    @Test
    void rowCount_success() {
        doReturn(1L).when(paymentDAOMock).count();

        assertEquals(1, paymentBs.rowCount());
    }

    @Test
    void findById_success() {
        Payment payment = new Payment(2, LocalDate.now(), "VISA", 150.00, 1234);
        doReturn(payment).when(paymentDAOMock).findById(anyInt());

        Payment paymentResult = paymentBs.findById(2);

        assertNotNull(paymentResult);
        assertEquals(2, paymentResult.getId());
    }

    @Test
    void findById_NoReturns() {
        doReturn(new Payment()).when(paymentDAOMock).findById(anyInt());

        Payment paymentResult = paymentBs.findById(2);

        assertNotNull(paymentResult);
    }

    @Test
    void findById_exception() {
        Throwable exception =
                assertThrows(PaymentException.class, () -> {
                    paymentBs.findById(-1);
                });
        assertEquals(FINDBYID_EXCEPTION_MSG, exception.getMessage());
    }

    @Test
    void findByType_success() {
        doReturn(Collections.singletonList(new Payment(2, LocalDate.now(), "VISA", 150.00, 1234))).when(paymentDAOMock).findByType(anyString());

        List<Payment> paymentResult = paymentBs.findByType("VISA");

        assertNotNull(paymentResult.get(0));
        assertEquals("VISA", paymentResult.get(0).getType());
    }

    @Test
    void findByType_NoReturns() {
        doReturn(Collections.singletonList(new Payment())).when(paymentDAOMock).findByType(anyString());

        List<Payment> paymentResult = paymentBs.findByType("VISA");

        assertNotNull(paymentResult.get(0));
    }

    @Test
    void findByType_exception() {
        Throwable exception =
                assertThrows(PaymentException.class, () -> {
                    paymentBs.findByType("");
                });
        assertEquals(FINDBYTYPE_EXCEPTION_MSG, exception.getMessage());
    }

    @Test
    void save_success() {
        Payment payment = new Payment(2, LocalDate.now(), "VISA", 150.00, 1234);

        doReturn(payment).when(paymentDAOMock).save(any(Payment.class));

        int result = paymentBs.save(payment);

        assertEquals(1, result);
    }

    @Test
    void save_exception() {
        Throwable exception =
                assertThrows(PaymentException.class, () -> {
                    paymentBs.save(new Payment());
                });
        assertEquals(SAVE_EXCEPTION_MSG, exception.getMessage());
    }


}