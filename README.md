# README #

#### Important: ####
* This project is ‘open book’, you may search the internet for help, and use features in your existing web application from the teaching sessions.
* If you find you are not going to complete all steps then focus on getting as many steps as possible implemented well. 

You are required to create a series of features which need to be built, unit-tested and integrated using REST. BitBucket(Git) will be used for version control.

In this assignment you are required to complete the following layers for the Payment system. The details of the classes and behaviour required are in the appropriate sections later in this document.
* __DTO/Entity layer:__ Create a DTO layer to represent the payment data from the Payment table. The DTO is an object that holds data. It is a JavaBean with instance variables and setter and getters. 
* __DAO Layer:__ Create a Spring Bean object to represent the DAO layer. The DAO Spring Bean is a class which has the CRUD operations like read and save.
* __Business/Service:__ Create a business/service layer that implements the business logic. This layer has to perform more operations than just calling a method from a DAO object e.g. parameter validation checks. The business/service layer is a Spring Bean class that contains business logic.
* __REST Web Service:__ This layer will contain the methods responsible for handling incoming web requests.